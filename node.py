from SocketServer import ThreadingMixIn

from threading import Thread
from datetime import datetime
from time import time, sleep
from socket import gethostbyname, gethostname
from xmlrpclib import ServerProxy
from SimpleXMLRPCServer import SimpleXMLRPCServer
from random import randint, choice, random

class AsyncXMLRPCServer(ThreadingMixIn,SimpleXMLRPCServer): pass

class Node(object):
	""" Class to implement unsecure base distributed
		calculations. May be extended to add synchronisation features."""
	DURATION = 20	# The duration of a single calculation session

	def __init__(self, hostname=None):
		""" Initalization function. Sets up the class and starts the server."""
		self.value = None
		self.address = gethostbyname('%s.local' % gethostname())
		self.port = randint(1024, 4096)
		self.hostname = 'http://%s:%d' % (self.address, self.port)
		self.server = AsyncXMLRPCServer((self.address, self.port), logRequests=False)
		self.thread = Thread(target=self.__loop)
		self.nodes = []
		self.started = False
		self.wantsToken = False
		self.thread.start()
		if hostname:
			self.__connect(hostname)

	def _log(self, msg):
		""" Intern method to log messages more nicely. """
		stamp = datetime.now().strftime('%H:%M:%S')
		print '[%s] [%s] %s' % (self.hostname[7:], stamp, msg)

	def __loop(self):
		""" The Server main 'loop'. """
		self._log('Started node')
		self.server.register_instance(self)
		self.server.register_function(self.join, 'Service.join')
		self.server.register_function(self.start, 'Service.start')
		self.server.register_function(self.leave, 'Service.leave')
		self.server.register_function(self.calculate, 'Service.caluclate')
		self.server.serve_forever()

	def __connect(self, hostname):
		""" Connects to a existing node network. """
		hosts = [hostname]
		while hosts:
			host = hosts.pop()
			client = ServerProxy(host)
			self.nodes.append(host)
			remote = client.Service.join(self.hostname)
			for node in remote:
				if not node in self.nodes and node != self.hostname and node not in hosts:
					hosts.append(node)

	def __disconnect(self):
		""" Disconnects from all connected nodes. """
		while self.nodes:
			node = self.nodes.pop()
			client = ServerProxy(node)
			client.Service.leave(self.hostname)

	def __calculation(self):
		""" Fire random calculations for the set duration. """
		while time() - self.started < Node.DURATION:
			sleep((int)(random()/10*Node.DURATION))
			self.wantsToken = True

	def _fire(self):
		""" Sends a random calculation to the node network. """
		method = choice(['sum', 'subtract', 'divide', 'multiply'])
		value = randint(1, 20)
		self._log('Fire %s, %d' % (method, value))
		self.calculate(method, value)
		for node in self.nodes:
			try:
				client = ServerProxy(node)
				client.Service.calculate(method, value)
			except Exception, e:
				pass

	def start(self, initValue):
		""" Starts the calculation session. """
		self.value = initValue
		self.started = time()
		Thread(target=self.__calculation).start()
		return True

	def join(self, hostname):
		""" Joins the nodes network. """
		self._log('%s joined' % hostname)
		self.nodes.append(hostname)
		return self.nodes

	def leave(self, hostname):
		""" Leaves the nodes network. """
		if hostname in self.nodes:
			self.nodes.remove(hostname)
			return True
		return False

	def calculate(self, operand, value):
		""" Let the node calculate something. """
		if operand == 'sum':
			self.value += value
		elif operand == 'sub':
			self.value -= value
		elif operand == 'div':
			self.value /= value
		elif operand == 'mul':
			self.value *= value
		self._log('= %d' % self.value)
		return True