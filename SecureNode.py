from node import Node
from Queue import PriorityQueue
from time import time, sleep
from threading import Thread

class SecureNode(Node):
	def __init__(self, hostname=None):
		super(SecureNode, self).__init__(hostname)
		self.queue = PriorityQueue()
		self.wanting = False
		self.accessing = False
		self.acks = 0

	def __sendOK(self, hostname):
		try:
			self.nodes[hostname].OK()
		except Exception, e:
			pass

	def OK(self):
		self.acks += 1

	def handle(self, hostname, timestamp):
		if not self.accessing and not self.wanting:
			self.__sendOK(hostname)
		elif self.accessing:
			self.queue.put((timestamp, hostname))
		elif self.wanting:
			if timestamp < self.wanting:
				self.__sendOK(hostname)
			else:
				self.queue.put((timestamp, hostname))

	def _fire(self):
		self.acks = 0
		self.wanting = time()
		for node in self.nodes:
			try:
				self.nodes[node].handle(self.hostname, self.wanting)
			except Exception, e:
				pass
		while self.acks < len(self.nodes):
			sleep(1)
		self.accessing = True
		super(SecureNode, self)._fire()
		self.accessing = False
		self.wanting = False
		while not self.queue.empty():
			self.__sendOK(self.queue.get()[1])
		self.queue = PriorityQueue()

def demo():
	""" For testing purposes only. """
	n1 = SecureNode()
	n2 = SecureNode(n1.hostname)
	n3 = SecureNode(n2.hostname)
	for n in [n1, n2, n3]:
		n.start(5)

demo()