from node import Node
from xmlrpclib import ServerProxy
from threading import Thread

from time import time, sleep

__author__  = "Niklas Bergmann"
__email__   = "niklas.bergmann@fkie.fraunhofer.de"
__version__ = "1.0"

class TokenNode(Node):
	""" A node using the Token Ring algorithm.

		Keyword arguments:
		hostname -- a hostname to connect to
	"""
	def __init__(self, hostname=None):
		super(TokenNode, self).__init__(hostname)
		self.hasToken = False if not hostname else True
		self.tokenVal = 0
		self.server.register_function(self.tokenReceive, 'Service.tokenReceive')
		self.tokenThread = Thread(target=self._handleToken)
		self.tokenThread.start()

	def _handleToken(self):
		""" Loop which triggers calculations. """
		while True:
			sleep(0.01)
			if self.wantsToken and self.hasToken:
				self._fire()
				self.wantsToken = False
			if self.hasToken:
				self._shootToken(self.tokenVal + 1)
				self.hasToken = False

	def _shootToken(self, token):
		""" Shots the token to the next node. """
		node = self._getNextNode()
		client = ServerProxy(node)
		client.Service.tokenReceive(token)

	def _getNextNode(self):
		""" Returnes the address of the next node
			in line fpr the token. """
		nodes = [self.hostname]
		nodes.extend(self.nodes)
		nodes.sort()
		index = nodes.index(self.hostname) + 1
		if index == len(nodes):
			index = 0
		return nodes[index]

	def tokenReceive(self, token):
		""" Recieve the token and set the corresponding
			values. """
		#self._log('Recieved token %d' % token)
		self.tokenVal = token
		self.hasToken = True
		return True

def demo(n, value):
	""" For testing purposes only. """
	last = None
	nodes = []
	for i in range(n):
		node = TokenNode(last)
		nodes.append(node)
		last = node.hostname
	for node in nodes:
		node.start(value)

#demo(5, 5)
node = TokenNode('http://localhost:4544')